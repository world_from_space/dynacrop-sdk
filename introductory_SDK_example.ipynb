{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introductory SDK Example\n",
    "\n",
    "A typical approach to use DynaCrop API is to create a polygon, representing an agricultural field, and obtain desired information for it. Say we've already [authenticated](authentication.html).\n",
    "\n",
    "In this example, we want to obtain the last available state of the field with regard to the [NDVI index](https://docs.dynacrop.space/docs/#/products?id=ndvi-normalized-difference-vegetation-index).\n",
    "\n",
    "First, we set up necessary imports for our example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "import geopandas as gpd\n",
    "\n",
    "from dynacrop import Polygon, Observation, Layer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Polygon setup\n",
    "To create a [Polygon](https://docs.dynacrop.space/docs/#/api?id=interface-for-polygon-management), we need a **valid polygon geometry** in the Well-Known-Text (WKT) format in **WGS 84** coordinate system. It can look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_field_wkt = \"POLYGON ((15.381691455695547 49.303972332109694, 15.38159608797287 49.3027037636056, 15.382111072758565 49.30221871416425, 15.380394458334196 49.30134810027714, 15.379993915339584 49.30103716295746, 15.379078387559279 49.2998680211984, 15.376846789658885 49.299420257525554, 15.376045703342243 49.29903468018158, 15.375301837593726 49.30257939242007, 15.381691455695547 49.303972332109694))\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to prepare it by loading a spatial file with `geopandas`. We then take any valid polygon geometry and convert it to WKT."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fields_gdf = gpd.read_file('test_field.shp')\n",
    "test_field_geometry = fields_gdf.loc[0].geometry\n",
    "\n",
    "test_field_wkt = test_field_geometry.to_wkt()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we simply call create on a Polygon class and fill in necessary parameters. Such, we create a polygon object that is automatically paired with a database of your api key and added among your fields."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<dynacrop.sdk.Polygon at 0x7f21ae87cbe0>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "field1 = Polygon.create(\n",
    "    test_field_wkt,\n",
    "    label=\"My test field\",\n",
    "    max_mean_cloud_cover=0.3,\n",
    "    smi_enabled=False\n",
    ")\n",
    "\n",
    "field1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "**DynaCrop API is asynchronous**. This means that our SDK request does not need to wait for the server to respond. The script is terminated right after it processes itself, i.e. sucessfully sends requests to the API. This is advantageous if we want to send many requests or the processing-heavy ones and do not want to wait.\n",
    "\n",
    "On the other hand, we may intend to continue processing further in our srcipt. Here, it is most likely undesired if the request (`Polygon.create` in our case) had not finished processing on the API side.\n",
    "\n",
    "Therefore, the SDK has a method `block_till_completed` to wait for the request to finish on the API side. We call this method right after firing the request:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'My test field is completed. Field id = 62509'"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "field1 = Polygon.create(\n",
    "    test_field_wkt,\n",
    "    label=\"My test field\",\n",
    "    max_mean_cloud_cover=0.3,\n",
    "    smi_enabled=False\n",
    ")\n",
    "\n",
    "field1.block_till_completed()\n",
    "\n",
    "# Now we wait :)\n",
    "\n",
    "# The status is garuanteed to be finished: completed or error\n",
    "f\"{field1.label} is {field1.status}. Field id = {field1.id}\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a processing request and obtain results\n",
    "Once our polygon is **completed**, we can establish a [processing request](https://docs.dynacrop.space/docs/#/services). We chose [Observation](https://docs.dynacrop.space/docs/#/services?id=observation) for this example since it is the easiest to grasp. The programming principle is quite similar to that behind `Polygon`. Observation gives us the actual state of the field to the latest available date.\n",
    "\n",
    "Observation, as any other processing request, has mandatory parameters. These are:\n",
    "\n",
    "- our previously created polygon (`field1`)\n",
    "- a [layer](https://docs.dynacrop.space/docs/#/products?id=available-indicesproducts), where we must use built-in enumeration `Layer` for layers. In most editors, this even gives us hinting.\n",
    "- dates from and to\n",
    "\n",
    "Again, we can opt to wait until the processing request is finished on the API side."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<dynacrop.Observation 1332218> \n",
      "Status: completed\n"
     ]
    }
   ],
   "source": [
    "observation1 = Observation.create(\n",
    "    polygon=field1, \n",
    "    layer=Layer.NMDI, \n",
    "    date_from=\"2020-06-01\", \n",
    "    date_to=\"2020-08-30\"\n",
    ")\n",
    "\n",
    "observation1.block_till_completed()\n",
    "\n",
    "print(observation1, f\"\\nStatus: {observation1.status}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the Observation has finished sucessfully, we can obtain results, which, again, is quite simple.\n",
    "\n",
    "The results can be saved in multiple formats. To stay within Python environment, we can decide between representations of: \n",
    "\n",
    "- *raster* - multidimensional `NumPy array` \n",
    "- *vector* - Geopandas `GeoDataFrame`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1, 55, 49)\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "<matplotlib.image.AxesImage at 0x7f21a9a08a90>"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAOMAAAD6CAYAAABTV/AfAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjQuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/Z1A+gAAAACXBIWXMAAAsTAAALEwEAmpwYAAAV2ElEQVR4nO3dfXTU1ZkH8O+XSUICCYS8AHkBAhWl2Fq1UUS0WhCl2lW77bp1+0JPOUvPdtti7bZi3W63p91dPKen1lN329LFLXtOt2qrFspqFa11axU1CCgvUgIKghACJMpbEpI8+0d+aCZknpmQMHOT+X7O4TD39/xm5snLkzt37tzfpZlBRDJvWKYTEJEuKkaRQKgYRQKhYhQJhIpRJBAqRpFA9KsYSc4juZVkPcnFA5WUSDbi6c4zkowB+DOAuQB2A3gRwM1mtjnRfcrKyqympua0nk8kU1q2vBTXzh+XH39CyfQ+Pd7atWsPmFl5z+M5fU/tHRcDqDezHQBA8j4ANwBIWIw1NTWoq6vrx1OKpN+Wi/Li2ucsOieuPezTffudJrmzt+P9eZlaBeCNbu3d0bGeT7yQZB3JusbGxn48ncjQdsbfwDGzpWZWa2a15eWn9MwiEunPy9Q9ACZ0a1dHx0QGlcaPjYprH9jdkpE8+tMzvghgKsnJJPMAfBLAyoFJSyT7nHbPaGbtJL8E4DEAMQD3mtmmActMJMv052UqzOwRAI8MUC4iWa1fxSgSojevGenGK/5ySly7bN6kuHbR1ib3/m9PL4hrF6eemksfhxMJhIpRJBAqRpFAaMwog0L3cSBjzGAmZ456RpFAqBhFAqFiFAmExoySEfZfH3DjzY/vimsXlLy7jKlwarF731hJvhtv3XzIjXe2dbjxzVPa4tqXumenTj2jSCBUjCKBUDGKBEJjRhkQrV+fENfOrSp0z+foPDeejdQzigRCxSgSCBWjSCA0ZpRePTjO/zt92UX+mkFufcuNl183yY23tySe62t986h73xHj/dzyJha58cNXlLrxMcdOxB8odk9PmXpGkUCoGEUCoWIUCYTGjFnizzOHx7WLy4cnOFMyRT2jSCBUjCKBUDGKBEJjxkFq//Wj3HgsN/46MaWV8Wv8dm075t5/Qqn/d7qwcoQbbzno71fRVt/sxktnVyeMscD/tbXiJJ973XnEDR/86lo3ftYd58cfqPSfLlXqGUUCoWIUCYSKUSQQGjMG4tUZ8fN+W3adSHBml1kX+esFZfBRzygSCBWjSCCSFiPJe0nuJ7mx27ESkqtJbov+H3Nm0xQZ+lIZM/4cwD0A/rvbscUAnjSzJSQXR+3bBj69wat+ln/tzp5ycuLnBefMLXHPz6/01+wd3fm2G6851x9zFp3rr+lr3XXYjR851ObGGfPvX5QXSxxMstdGTt5oN95xvN2Nd3a6YeTOW+OfcJqS9oxm9n8Ael719QYAy6PbywHcOLBpiWSf0x0zjjOzvdHtfQDGJTqR5EKSdSTrGhsbT/PpRIa+fr+BY2YGwJz4UjOrNbPa8vLy/j6dyJB1uvOMDSQrzGwvyQoA+wcyqVDtuPzdceD+A/64o6xEU7jSN6fbM64EMD+6PR/AioFJRyR7pTK18UsAzwE4h+RukgsALAEwl+Q2AFdFbRHph6Svpczs5gShOQOci0hWy+qBzaYPxq97O/CWvy9fSdG7c18TJ/rXkCmZVuzGGzc2ufFk+9Zbhz8Ztq/ev7bo2X812Y033ODPc+Z83X+b4NmNrW58WoU/5h75xvGEsar3F7v3jZUXuPGOWWPd+Pg5CScHzih9HE4kECpGkUCoGEUCMaTGjA9X+H9bzq4cUl+uDDHqGUUCoWIUCUTQr9saNs+Ma4/+6S73/Nrp/rKl1hZ/OuDAbv/t9iMt78ZHFTtLfADkneMv8ay4ZoIbP/HITjceu8K/PuDZl45344em5rrx9hz/e1Uy2d9W7YIGf2qjcJT/q1c8KfESsbwkUxcnkizvypnoLx9rz/O/N9e3fSuuvTLvu+75qVLPKBIIFaNIIFSMIoFI65jx+Ikt2LTv4oTxskP+OCzd8v2hg8iAUs8oEggVo0ggVIwigUjrmLElh3jV2WrswbJL3fv/6y3+3NfRgio3PuV7b7jxkaPj56cONyW+xH7VHP+52rb6S6SaVr3uxsde6T/+sXL/R7ev0p8zbRjtX0px1m/8+3fMTbxlGwCcPdnfsm7fo/6c8fBxibec40x/iVNuvX+ZyqPj/TcD8pv8pXQ/q38irt2A+Pa46c+5909EPaNIIFSMIoFQMYoEIujPpooMRo+2xM+lfyT/hZTup55RJBAqRpFAqBhFApHWMeMbxydh0aZlCeP3nv059/6P1fhzVxNb/XVsEz8zxY3vnhq/Bu/CPyb+W9X6O3+eLG+8v2Vb+aX+mrwjs8vc+Kvv8dcLFh/1/86e9aY/19Y52r9/Z4H/OeJh4/yvL9mlKHPOSrytW8to/9d2uLedHIDCV/zfExvlf29O5Prxic09DvhLS9+hnlEkECpGkUCoGEUCkdYx43kjClB3/rnvtKvXb0rn05/i1an+uEtkIHhreLtTzygSCBWjSCBS2Z9xAsmnSG4muYnkouh4CcnVJLdF//vXJhQRVypjxnYAXzOzl0gWAVhLcjWAzwF40syWkFwMYDGA2/ry5DUbpsW1fzJ9rnv+59uecOO5HebGG8fHr9Ebdcz/W3SiOPG3J9k84rAk1/bEmDw3XPTMITc+Df6WbXmJd1QDALQlSe9EsT8PmNfor4fs3O6vKWxv9demNj2a+LqxBWP95DuvneTHh/s/99gLjW68bI3/za3ocU3b2A09P5va+/c2ac9oZnvN7KXo9mEAWwBUAbgBwPLotOUAbkz2WCKSWJ/GjCRrAFwA4HkA48xsbxTaB6DX5dckF5KsI1nX2Oj/xRHJZikXI8lCAA8CuMXM4l6DmJkB6PU1opktNbNaM6stLy/vV7IiQ1lK84wkc9FViL8ws4eiww0kK8xsL8kKAP6+0iJDVP4Pdg/I46TybioBLAOwxcx+0C20EsD86PZ8ACsGJCORLJVKzzgLwGcAvEJyfXTsmwCWAHiA5AIAOwHcdEYyFMkSSYvRzJ5BovdigTkDm45I9sroNXBeu+rxuHb9nqvd8xeVPe7Gz9863I235fvzkD3FjieeC7MZSd6M2ubPs7VV+ntJxpLNQz7nX5eVhf6au2Gb/XnMZPtL7rki8XVNAaDqsD8PW/GhCje+64k9CWPFl/t7U9oJfw4z50ji6+ECwLErx7rxwpl/cuOnSx+HEwmEilEkECpGkUAM6eum9nWMKNKbMzVG7Ek9o0ggVIwigVAxigQio2PGc/+xJq79vmV3uuePaPYfr+cavqZSf5+9iav862euuWdrwtiMz/nXYH36y8Vu/JJ1/nrB2KZmN35kwwH//l84x40XJJlr69h3zI2X7ity461T/HnGnF1H3Lin6Q+J5yABoOTqiW7ckqx7LbxpbZ9zGgjqGUUCoWIUCYSKUSQQQ3qeUaQ3w764MdMp9Eo9o0ggVIwigVAxigQio2PGxpV/G9eeTH9uqmFkkutdvjf+Wp4jW/y5vGO1/pq9mXe8P2Gso8xfjwj4c5wN1e1uvPppPx7L9/cgPDban0trvbzYjRf/sdmN59f784SWZB7z4Hr/SoH1uxJfl/XpNf5j/93bO+La1auPuueHQj2jSCBUjCKBUDGKBCKjY8Z1B5+Ja//IrunT/SsPa72inGqwjBF7Us8oEggVo0ggVIwigQjqs6k/P/wxN76sdVlce0WZvw/ftU2vu/FtF/rzVZ0XJr4O68680e59X+x4rxv/7Fj/uiqVM/1rdzb9dZkbb89JspbzobfcOMb5eyBag79HYedxf540d4T/qzfrmsTXpS18On4t56Vb/a91sFDPKBIIFaNIIFSMIoEIaswoksjIpQ3v3L40g3mcSeoZRQKhYhQJRCo7F+eTfIHkBpKbSH4nOj6Z5PMk60neT9Lfw0xEXKmMGVsBzDazIyRzATxD8lEAtwK4y8zuI/kTAAsA/Lg/ybyn4DU/viW+3t875k33/B+NusyNT4/tcOMPHU38WdmOVn894QdHbnDjnz/8LTf+ldlL3fh5xxvc+NEcP7+qc4vdeKwp8XpCIPn+jx0XlrjxQ4/tduMTr6py40NR0p7RupxcSZob/TMAswH8Ojq+HMCNZyJBkWyR0piRZIzkegD7AawGsB1As5md/JjFbgC9/ikjuZBkHcm6xkZ/dbdINkupGM2sw8zOB1AN4GIA01J9AjNbama1ZlZbXp5k622RLNaneUYzayb5FICZAIpJ5kS9YzUAfwOE07DspWeSnyRDUs63t2c6hbRL5d3UcpLF0e0CAHMBbAHwFIBPRKfNB7DiDOUokhVS6RkrACwnGUNX8T5gZqtIbgZwH8nvAVgHYJn3ICLiS1qMZvYygAt6Ob4DXeNHERkAQX029fKc9XHtV85pdc9fFzvbjbd3+HNt+82fC2vYPC9h7Lpafy/JFku8FhIApo7e7MYrOpvd+LS9/tfWmudfH6j+vGTXD/J/Naau9z/jkbvlbTdecveFce3iC59Nks/Qp4/DiQRCxSgSCBWjSCCCGjNK9tAY8VTqGUUCoWIUCYSKUSQQQY0Zv8zH4trPotY9fyya3Pimzve48YcP+VdTueaD308Ym9Pxsnvfb7x1qxv/9ph73Hgb/XnE3aUtbnzt6FI3/rs2/2u/Ls//XHBOhz+POPJuf21q5RfdcFZSzygSCBWjSCBUjCKBCGrMKENX5WODc8/EdFLPKBIIFaNIIFSMIoEIesy4d6Sf3nPt57nxTw970o3/eox/bdCPtr+UMPabHH9d9dHD/v6Kr5T6e0uWwt8/Mb/Q3//w3/b485xTxmxy45ccbnbjhwr99ZA1X32/G5dTqWcUCYSKUSQQKkaRQAQ9ZpTBK3femkynMOioZxQJhIpRJBAqRpFABD1mXJMz1Y3/6c0r3fj2oslu/DvD73Xj1W8nnksrLD3m3veuCXe48f9s+Qs3Pj/vETeezI2VD7rxUvrzmL8d4c+DLqj391fEGD8sp1LPKBIIFaNIIFSMIoEIeswog0f1VM0r9pd6RpFApFyMJGMk15FcFbUnk3yeZD3J+0n62xKJiKsvPeMidO1YfNKdAO4ys7MANAFYMJCJiWSblMaMJKsBXAfgXwDcSpIAZgP4m+iU5QD+GcCPBzK5ZGv6xhftdOPNbf7+i/kxf03ea6MTX7v0U29tcO/71nC68YMn/Ouavpbvr4f8wtY9bvzjb7zixn97pf+1/++wi9x4Tof//NJ3qfaMPwTwDQCdUbsUQLOZnVzhuhtAVW93JLmQZB3JusbGxv7kKjKkJS1Gkh8FsN/M1p7OE5jZUjOrNbPa8vLy03kIkayQysvUWQCuJ3ktgHwAowDcDaCYZE7UO1YD0OsWkX5IWoxmdjuA2wGA5JUA/sHMPkXyVwA+AeA+APMBrBjo5BZzZVx7iV0/0E8hp2n8tOcyncKQ0595xtvQ9WZOPbrGkMsGJiWR7NSnT+CY2R8A/CG6vQOAf4k0EUnZoPo43M8OfjqubVsvcc9fMuOTbnzsQf/5PrymM3Gw05+62HC5/9j/kfNTN767w/8MRctwJzcAsSN+vE2f0QiOPg4nEggVo0ggVIwigRhUY0bJnHtjP8p0CkOeekaRQKgYRQKhYhQJxKAaM/acV/zmzPnu+U0scOPbSvy5uKmrEy/RyqsqdO97Xq6/fGvjDP+5n+AH3PiJif4SqaLKE2781j3fdeP5BU3xB/wVXTIA1DOKBELFKBIIFaNIIAbVmFHS57WxH890CllHPaNIIFSMIoFQMYoEYlCNGX9yQfw4Zthx//ztI0a58XHH2934wUWJt0Wr+v1R976drxxy41U1/sW5PjzmZTfeFBvhxh/kh9z43kmz3bikn3pGkUCoGEUCoWIUCcSgGjPKu77I1X5cl7gZdNQzigRCxSgSCBWjSCAG1Zjxs3t+GNe+5aw73fPL7LAbf6ao0o138M2Esfar/Xm+kUeL3PiGyfHrDa8a+WJc+2b33jIUqWcUCYSKUSQQKkaRQAyqMeO+qbPi2kv8nbAzqvx9z7rxq9KUhwwe6hlFAqFiFAlESi9TSb4O4DCADgDtZlZLsgTA/QBqALwO4CYza0r0GCLi68uY8cNmdqBbezGAJ81sCcnFUfu2Ac0uiQmd/prBe1pu6tfjT8/7RVz7+uHPv9vwpxFF+qw/L1NvALA8ur0cwI39zkYki6VajAbgcZJrSS6Mjo0zs73R7X0AxvV2R5ILSdaRrGtsbOxnuiJDV6ovUy8zsz0kxwJYTfLV7kEzM5K9TjSY2VIASwGgtrY24MkIkcxKqRjNbE/0/36SDwO4GEADyQoz20uyAsD+M5hnWjw38is9jvRsi5w5SV+mkhxJsujkbQBXA9gIYCWAkzvPzAew4kwlKZINUukZxwF4mOTJ8//HzH5H8kUAD5BcAGAngP69dSmS5ZIWo5ntAHDK/mRmdhDAnDORlEg2GlSfTe3pa9u/78Y/NPlhN/5Arr8eUiSd9HE4kUCoGEUCoWIUCcSgHjMmozGhDCbqGUUCoWIUCYSKUSQQg3rMuKL8S3HtGWPqMpSJSP+pZxQJhIpRJBAqRpFA0Cx9631JNqJrhUcZgANJTs+UkHMDws4v5NyAcPKbZGblPQ+mtRjfeVKyzsxq0/7EKQg5NyDs/ELODQg/P71MFQmEilEkEJkqxqUZet5UhJwbEHZ+IecGBJ5fRsaMInIqvUwVCYSKUSQQaS1GkvNIbiVZH+3PkVEk7yW5n+TGbsdKSK4muS36f0yGcptA8imSm0luIrkosPzySb5AckOU33ei45NJPh/9jO8nmZeJ/KJcYiTXkVwVWm69SVsxkowB+HcAHwEwHcDNJKen6/kT+DmAeT2OndzQZyqAJ6N2JrQD+JqZTQdwCYC/j75foeTXCmC2mX0AwPkA5pG8BMCdAO4ys7MANAFYkKH8AGARgC3d2iHldiozS8s/ADMBPNatfTuA29P1/E5eNQA2dmtvBVAR3a4AsDXTOUa5rAAwN8T8AIwA8BKAGej6hEtObz/zNOdUja4/VrMBrALAUHJL9C+dL1OrALzRrb07OhaalDb0SSeSNQAuAPA8Asovehm4Hl1bO6wGsB1As5m1R6dk8mf8QwDfANAZtUsRTm690hs4Duv6E5rRuR+ShQAeBHCLmb3dPZbp/Mysw8zOR1cvdDGAaZnKpTuSHwWw38zWZjqXvkjn4uI9ACZ0a1dHx0ITzIY+JHPRVYi/MLOHQsvvJDNrJvkUul76FZPMiXqgTP2MZwG4nuS1APIBjAJwdyC5JZTOnvFFAFOjd7TyAHwSXZvnhCaIDX3YtbnJMgBbzOwH3UKh5FdOsji6XYCu8ewWAE8B+EQm8zOz282s2sxq0PV79nsz+1QIubnSPKi+FsCf0TW2uCPTA2YAvwSwF8AJdI0hFqBrbPEkgG0AngBQkqHcLkPXS9CXAayP/l0bUH7nAVgX5bcRwD9Fx6cAeAFAPYBfARie4Z/xlQBWhZhbz3/6OJxIIPQGjkggVIwigVAxigRCxSgSCBWjSCBUjCKBUDGKBOL/AZLP1tElsURKAAAAAElFTkSuQmCC",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "observation_raster = observation1.as_array()\n",
    "print(observation_raster.shape)\n",
    "\n",
    "# When displaying, we need to slice the first dimension\n",
    "plt.imshow(observation_raster[0], cmap='turbo')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>geometry</th>\n",
       "      <th>fid</th>\n",
       "      <th>DN</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>POLYGON ((15.38156 49.30397, 15.38169 49.30397...</td>\n",
       "      <td>out.0</td>\n",
       "      <td>0.628972</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>POLYGON ((15.38114 49.30388, 15.38128 49.30388...</td>\n",
       "      <td>out.1</td>\n",
       "      <td>0.638143</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>POLYGON ((15.38142 49.30397, 15.38156 49.30397...</td>\n",
       "      <td>out.2</td>\n",
       "      <td>0.642138</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>POLYGON ((15.38072 49.30379, 15.38100 49.30379...</td>\n",
       "      <td>out.3</td>\n",
       "      <td>0.639286</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>POLYGON ((15.38100 49.30379, 15.38128 49.30379...</td>\n",
       "      <td>out.4</td>\n",
       "      <td>0.630317</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>...</th>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>406</th>\n",
       "      <td>POLYGON ((15.37655 49.29948, 15.37683 49.29948...</td>\n",
       "      <td>out.408</td>\n",
       "      <td>0.603687</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>407</th>\n",
       "      <td>POLYGON ((15.37683 49.29948, 15.37697 49.29948...</td>\n",
       "      <td>out.409</td>\n",
       "      <td>0.614668</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>408</th>\n",
       "      <td>POLYGON ((15.37627 49.29930, 15.37655 49.29930...</td>\n",
       "      <td>out.410</td>\n",
       "      <td>0.606111</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>409</th>\n",
       "      <td>POLYGON ((15.37600 49.29930, 15.37627 49.29930...</td>\n",
       "      <td>out.412</td>\n",
       "      <td>0.597313</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>410</th>\n",
       "      <td>POLYGON ((15.37600 49.29912, 15.37614 49.29912...</td>\n",
       "      <td>out.413</td>\n",
       "      <td>0.617317</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>411 rows × 3 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "                                              geometry      fid        DN\n",
       "0    POLYGON ((15.38156 49.30397, 15.38169 49.30397...    out.0  0.628972\n",
       "1    POLYGON ((15.38114 49.30388, 15.38128 49.30388...    out.1  0.638143\n",
       "2    POLYGON ((15.38142 49.30397, 15.38156 49.30397...    out.2  0.642138\n",
       "3    POLYGON ((15.38072 49.30379, 15.38100 49.30379...    out.3  0.639286\n",
       "4    POLYGON ((15.38100 49.30379, 15.38128 49.30379...    out.4  0.630317\n",
       "..                                                 ...      ...       ...\n",
       "406  POLYGON ((15.37655 49.29948, 15.37683 49.29948...  out.408  0.603687\n",
       "407  POLYGON ((15.37683 49.29948, 15.37697 49.29948...  out.409  0.614668\n",
       "408  POLYGON ((15.37627 49.29930, 15.37655 49.29930...  out.410  0.606111\n",
       "409  POLYGON ((15.37600 49.29930, 15.37627 49.29930...  out.412  0.597313\n",
       "410  POLYGON ((15.37600 49.29912, 15.37614 49.29912...  out.413  0.617317\n",
       "\n",
       "[411 rows x 3 columns]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "observation_vector = observation1.as_geodataframe()\n",
    "\n",
    "# The GeoDataFrame composes of particular pixels or dissolved parts \n",
    "# depending on the Digital Number (DN), i.e. the pixel value.\n",
    "observation_vector"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simple external visualization\n",
    "\n",
    "Additionally, DynaCrop API enables us to create a simple visualization of the result in a web browser using *tiles*. The result will be shown in an original [DynaCrop color scheme](https://docs.dynacrop.space/docs/#/colors), depending on the used Layer (NDVI in our case)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This opens a default web browser with Leaflet interface and \n",
    "# zooms to the result over an ortophoto map.\n",
    "observation1.preview()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Store results\n",
    "We can store results in various formats using `observation1.save*` methods. Here we are going to store the result as raster, in the GeoTIFF format. We just need to specify the path."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "observation1.save_tiff('./observation_test_field.tiff')"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "d03eb6886f203e1abf9062a11546a222bd43ef5c9f7f5785a59c712837e25727"
  },
  "kernelspec": {
   "display_name": "Python 3.8.10 64-bit ('wfsvenv': venv)",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
