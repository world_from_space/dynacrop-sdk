from __future__ import annotations

from typing import Any, Iterable, List, Optional, Set, Tuple

from geopandas import GeoDataFrame  # type: ignore
from shapely import wkt  # type: ignore

from .api_handles import APIObject
from .attrs import PolygonAttrs, UserAttrs
from .constants import Layer
from .processing_request_base import (
    FieldZonationBase,
    JSONProcessingRequest,
    ObservationBase,
)


class User(APIObject, UserAttrs):
    """User endpoint object"""

    _editable_attrs: Set[Any] = set()
    _endpoint: str = "user"

    def __init__(self):
        """Creates User endpoint object"""
        super(User, self).__init__()

    def __repr__(self) -> str:
        """Prints information about the user.

        Returns:
            str: The information.
        """
        return str(self.__dict__["_data"])


class Polygon(APIObject, PolygonAttrs):
    """Polygon endpoint object"""

    _editable_attrs: Set[Any] = {"max_mean_cloud_cover", "label", "smi_enabled"}
    _endpoint: str = "polygons"

    @classmethod
    def create(  # type: ignore[override]
        cls,
        geometry: str,
        label: Optional[str] = None,
        max_mean_cloud_cover: Optional[int] = None,
        smi_enabled: bool = False,
        **kwargs,
    ) -> Polygon:
        """Creates Polygon endpoint object.

        Args:
            geometry (str): Valid polygon shape in
                Well Known Text (WKT) representation
                (see: https://en.wikipedia.org/wiki/Well-\
                    known_text_representation_of_geometry).
            label (Optional[str], optional): Description to the Polygon
                (user field). Defaults to None.
            max_mean_cloud_cover (Optional[int], optional): Maximum mean cloud
                coverage in decimal percentage (i.e. 0.3). Defaults to None.
            smi_enabled (bool, optional): To enable Soil Moisture Index
                pre-computation. Defaults to False.

        Returns:
            Polygon: APIObject instantiated into Polygon child.
        """
        return super(Polygon, cls).create(
            geometry=geometry,
            label=label,
            max_mean_cloud_cover=max_mean_cloud_cover,
            smi_enabled=smi_enabled,
        )  # type: ignore

    @staticmethod
    def as_geodataframe(
        polygons: list,
        fields: list = [
            "id",
            "area",
            "last_valid_observation",
            "smi_enabled",
            "max_mean_cloud_cover",
            "label",
            "valid_observations",
            "cloud_cover_percent",
            "last_updated",
            "created_date",
            "completed_date",
            "last_updated",
        ],
        iterables_to_strings: Optional[bool] = True,
    ) -> GeoDataFrame:
        """Create a Geopandas GeoDataFrame from a list of Polygon objects. This
        can be then saved to a file by GeoDataFrame `to_file` method.

        Args:
            polygons (list): A list of polygons to create the GeoDataFrame
                from.
            fields (Optional[list], optional): Polygon attributes that will be
                represented as fields in the GeoDataFrame. Iterable attributes
                are converted to string for save . Defaults to ["id", "area",
                "last_valid_observation", "smi_enabled", "max_mean_cloud_cover"
                , "label", "valid_observations", "cloud_cover_percent",
                "last_updated", "created_date", "completed_date", "last_updated
                "].
            iterables_to_strings (Optional[bool], optional): If True, all
                iterable Polygon attributes will be converted to strings
                for saving purposes. Spatial files usually reject saving Python
                iterables to attribute tables.

        Returns:
            GeoDataFrame: GeoDataFrame of polygons.
        """
        data = [[getattr(p, field) for field in fields] for p in polygons]

        if iterables_to_strings:
            data = [
                [str(attr) if isinstance(attr, Iterable) else attr for attr in attrs]
                for attrs in data
            ]

        geometry = [wkt.loads(p.geometry) for p in polygons]
        return GeoDataFrame(data=data, geometry=geometry, columns=fields)


class PolygonV3(Polygon):
    """Polygon V3 API endpoint object"""

    _apiversion = "v3"
    _editable_attrs: Set[Any] = {"options", "plan"}


class TimeSeries(JSONProcessingRequest):
    """Time series endpoint object. Time series is the service of
    DynaCrop API."""

    rendering_type = "time_series"

    @classmethod
    def create(  # type: ignore[override]
        cls,  # type: ignore
        polygon: Polygon,
        layer: Layer,
        date_from: str,
        date_to: str,
    ) -> TimeSeries:
        """Creates a TimeSeries endpoint object.

        Args:
            polygon (Polygon): Polygon endpoint object.
            layer (str): Should be Layer enumration. One of the DynaCrop API
                layers. See https://dynacrop.worldfromspace.cz/docs/#/products
                for further information.
            date_from (str): Date to record time series from.
            date_to (str): Date to record time series to.

        Returns:
            APIObject: APIObject instatiated into TimeSeries child.
        """
        return super(TimeSeries, cls).create(
            polygon_id=polygon.id,
            rendering_type=cls.rendering_type,
            layer=layer.value,
            date_from=date_from,
            date_to=date_to,
        )  # type: ignore


class TimeSeriesV3(JSONProcessingRequest):
    _apiversion = "v3"
    _endpoint: str = "time_series"

    @classmethod
    def create(  # type: ignore[override]
        cls,  # type: ignore
        polygon: PolygonV3,
        product: Tuple[str, str],
        date_from: str,
        date_to: str,
    ) -> TimeSeriesV3:
        """Creates a TimeSeries endpoint object using V3 API.

        Args:
            polygon (PolygonV3): PolygonV3 endpoint object.
            product ([str, str]): Selected source and product
                (e.g., [Sentinel-1, NDVI]). See
                https://dynacrop.worldfromspace.cz/docs/#/products
                for further information.
            date_from (str): Date to record time series from.
            date_to (str): Date to record time series to.

        Returns:
            APIObject: APIObject instatiated into TimeSeriesV3 child.
        """
        return super(TimeSeriesV3, cls).create(
            polygon_id=polygon.id,
            product=product,
            date_from=date_from,
            date_to=date_to,
        )  # type: ignore


class Observation(ObservationBase):
    """Observation endpoint object. Observation is the service of
    DynaCrop API."""

    rendering_type = "observation"

    @classmethod
    def create(  # type: ignore[override]
        cls,
        polygon: Polygon,
        layer: Layer,
        date_from: str,
        date_to: str,
    ) -> Observation:
        """Creates Observation endpoint object.

        Args:
            polygon (Polygon): Polygon endpoint object.
            layer (str): Should be Layer enumration. One of the DynaCrop API
                layers. See https://dynacrop.worldfromspace.cz/docs/#/products
                for further information.
            date_from (str): Date to watch for observation from.
            date_to (str): Date to watch for observation to.

        Returns:
            APIObject: APIObject instatiated into Observation child.
        """
        return super(Observation, cls).create(
            polygon_id=polygon.id,
            rendering_type=cls.rendering_type,
            layer=layer.value,
            date_from=date_from,
            date_to=date_to,
        )  # type: ignore


class ObservationV3(ObservationBase):
    """Observation V3 API endpoint object"""

    _apiversion = "v3"
    _endpoint: str = "observations"

    @classmethod
    def create(  # type: ignore[override]
        cls,
        polygon: Polygon,
        product: Tuple[str, str],
        date_from: str,
        date_to: str,
    ) -> ObservationV3:
        return super(ObservationV3, cls).create(
            polygon_id=polygon.id,
            product=product,
            date_from=date_from,
            date_to=date_to,
        )  # type: ignore


class FieldZonation(FieldZonationBase):
    """Field zonation endpoint object. Field zonation is the service of
    DynaCrop API."""

    rendering_type = "field_zonation"

    @classmethod
    def create(  # type: ignore[override]
        cls,
        polygon: Polygon,
        layer: Layer,
        date_from: str,
        date_to: str,
        number_of_zones: int = 3,
    ) -> FieldZonation:
        """Creates Field zonation endpoint object.

        Args:
            polygon (Polygon): Polygon endpoint object.
            layer (str): Should be Layer enumration. One of the DynaCrop API
                layers. See https://dynacrop.worldfromspace.cz/docs/#/products
                for further information.
            date_from (str): Date to compute field zonation from.
            date_to (str): Date to compute field zonation to.
            number_of_zones (int): Number of zones to separate the field to.
                The number of zones must be one of 3, 5, 10, 20, 255.

        Returns:
            APIObject: APIObject instatiated into FieldZonation child.
        """
        return super(FieldZonation, cls).create(
            polygon_id=polygon.id,
            rendering_type=cls.rendering_type,
            layer=layer.value,
            date_from=date_from,
            date_to=date_to,
            number_of_zones=str(number_of_zones),
        )  # type: ignore


class FieldZonationByMedian(FieldZonationBase):
    """Field zonation by median endpoint object. Field zonation by median
    is the service of DynaCrop API."""

    rendering_type = "field_zonation_by_median"

    @classmethod
    def create(  # type: ignore[override]
        cls,
        polygon: Polygon,
        layer: Layer,
        date_from: str,
        date_to: str,
        thresholds: Optional[list] = None,
    ) -> FieldZonationByMedian:
        """Creates Field zonation by median endpoint object.

        Args:
            polygon (Polygon): Polygon endpoint object.
            layer (str): Should be Layer enumration. One of the DynaCrop API
                layers. See https://dynacrop.worldfromspace.cz/docs/#/products
                for further information.
            date_from (str): Date to compute field zonation by median from.
            date_to (str): Date to compute field zonation by median to.
            thresholds (list): Thresholds to zone the field in between.
                The number of thresholds must be one of 2, 4, 9, 19, 254.

        Returns:
            APIObject: APIObject instatiated into FieldZonationByMedian child.
        """
        return super(FieldZonationByMedian, cls).create(
            polygon_id=polygon.id,
            rendering_type=cls.rendering_type,
            layer=layer.value,
            date_from=date_from,
            date_to=date_to,
            thresholds=thresholds,
        )  # type: ignore


class FieldZonationByMedianV3(FieldZonationBase):
    """Field zonation by median V3 API endpoint object"""

    _apiversion = "v3"
    _endpoint: str = "field_zonations_by_median"

    @classmethod
    def create(  # type: ignore[override]
        cls,
        polygon: PolygonV3,
        product: Tuple[str, str],
        dates_from: List[str],
        dates_to: List[str],
        thresholds: List[float] = [],
        inverse: bool = False,
    ) -> FieldZonationByMedianV3:
        """Creates Field zonation by median endpoint object using V3 API.

        Args:
            polygon (PolygonV3): Polygon endpoint object.
            product ([str, str]): Selected source and product
                (e.g., [Sentinel-1, NDVI]). See
                https://dynacrop.worldfromspace.cz/docs/#/products
                for further information.
            dates_from ([str]): List of interval start dates to use for zonation.
            dates_to ([str]): List of interval end dates to use for zonation.
            thresholds (list): Thresholds to zone the field in between.
                The number of thresholds must be one of 2, 4, 9, 19, 254.
        Returns:
            APIObject: APIObject instatiated into FieldZonationByMedianV3 child.
        """
        return super(FieldZonationByMedianV3, cls).create(
            polygon_id=polygon.id,
            product=product,
            dates_from=dates_from,
            dates_to=dates_to,
            thresholds=thresholds,
            inverse=inverse,
        )  # type: ignore


class FieldZonationQuantileV3(FieldZonationBase):
    """Field zonation by quantile V3 API endpoint object"""

    _apiversion = "v3"
    _endpoint: str = "field_zonations_quantile"

    @classmethod
    def create(  # type: ignore[override]
        cls,
        polygon: PolygonV3,
        product: Tuple[str, str],
        dates_from: List[str],
        dates_to: List[str],
        number_of_zones: int,
        inverse: bool = False,
    ) -> FieldZonationQuantileV3:
        """Creates Field zonation by quantile endpoint object using V3 API.

        Args:
            polygon (PolygonV3): Polygon endpoint object.
            product ([str, str]): Selected source and product
                (e.g., [Sentinel-1, NDVI]). See
                https://dynacrop.worldfromspace.cz/docs/#/products
                for further information.
            dates_from ([str]): List of interval start dates to use for zonation.
            dates_to ([str]): List of interval end dates to use for zonation.
            number_of_zones (int): Number of zones to create.
                The number of thresholds must be one of 2, 4, 9, 19, 254.
            inverse (bool): Invert zone values (lowest index will be
                the highest class and vice versa).
        Returns:
            APIObject: APIObject instatiated into FieldZonationQuantileV3 child.
        """
        return super(FieldZonationQuantileV3, cls).create(
            polygon_id=polygon.id,
            product=product,
            dates_from=dates_from,
            dates_to=dates_to,
            number_of_zones=number_of_zones,
            inverse=inverse,
        )  # type: ignore
