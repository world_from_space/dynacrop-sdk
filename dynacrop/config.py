from __future__ import annotations

import json
import os
from json import JSONDecodeError
from threading import Lock
from typing import Any, Optional

from .constants import CONFIG_PATH, CONFIG_FILENAME
from .exceptions import AuthenticationError


class ConfigMeta(type):
    _instances = {}  # type: ignore
    _lock: Lock = Lock()  # type: ignore

    def __call__(cls, *args: Any, **kwargs: Any) -> Any:
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
            return cls._instances[cls]


class Config(metaclass=ConfigMeta):
    """Config helper class."""

    def __init__(self):
        self._api_key: Optional[str] = None
        self._base_url: Optional[str] = "https://api.dynacrop.space/api"

    @property
    def api_key(self) -> Optional[str]:
        if not self._api_key:
            self.load()
        return self._api_key

    @property
    def base_url(self) -> Optional[str]:
        if not self._base_url:
            self.load()
        return self._base_url

    @api_key.setter
    def api_key(self, value):
        self._api_key = value

    @base_url.setter
    def base_url(self, value):
        self._base_url = value

    def load(self):
        try:
            with open(CONFIG_FILENAME) as config_read:
                json_data = json.load(config_read)
                self._api_key = json_data.get("api_key", self._api_key),
                self._base_url = json_data.get("base_url", self._api_key),
        except FileNotFoundError:
            raise AuthenticationError()
        except JSONDecodeError:
            raise

    def save(self):
        """Saves configuration to the JSON config file."""
        # Create config folder
        if not os.path.isdir(CONFIG_PATH):
            os.mkdir(CONFIG_PATH)

        with open(CONFIG_FILENAME, "w") as config_write:
            config_load = {
                "api_key": self.api_key,
                "base_url": self.base_url,
            }
            json.dump(config_load, config_write)

    def __str__(self) -> str:
        """Returns structured configuration information."""
        return str(self.__dict__)
