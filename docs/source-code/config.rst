SDK authentication and configuration
====================================

dynacrop.config module
----------------------

.. autoclass:: dynacrop.config.Config
   :members:
   :undoc-members:
   :show-inheritance: