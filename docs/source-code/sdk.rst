Elementary API endpoints
========================

dyancrop.sdk module
-------------------

.. automodule:: dynacrop.sdk
   :members:
   :undoc-members:
   :show-inheritance:


dynacrop.processing_request_base module
---------------------------------------

.. autoclass:: dynacrop.processing_request_base.ProcessingRequest
   :members:
   :undoc-members:
   :show-inheritance: