Supportive handles for API
==========================

dynacrop.api_handles module
---------------------------

.. autoclass:: dynacrop.api_handles.APIObject
   :members:
   :undoc-members:
   :special-members: __eq__, __setattr__, __getattr__, __dir__


.. autoclass:: dynacrop.api_handles.APIObjectIterator
   :members:
   :undoc-members:
   :special-members: __iter__, __next__

