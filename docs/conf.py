# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath("../."))

master_doc = "index"

# -- Project information -----------------------------------------------------

project = "DynaCrop API SDK"
copyright = "2023, World from Space"
author = "World from Space"

# The full version, including alpha/beta/rc tags
release = "0.1.0"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.autosectionlabel",
    "nbsphinx",
    "nbsphinx_link",
]
# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    "_build",
    "Thumbs.db",
    ".DS_Store",
    "__init__.py",
    "../sdk/__pycache__",
    "../sdk/config.json",
    "docs",
    "conf.py",
    "../playground.py",
    "../dynacrop/attrs/*",
]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "pydata_sphinx_theme"
autodoc_member_order = "bysource"
# html_sidebars = {'**': ['searchbox.html',
#                         'globaltoc.html',
#                         'relations.html',
#                         ]}
html_sidebars = {"**": ["search-field", "sidebar-nav-bs"]}
html_logo = "dynacrop-logo.svg"
html_theme_options = {
    "show_prev_next": False,
    "footer_items": ["copyright"],
    "navigation_depth": 3,
    "collapse_navigation": True,
    "show_toc_level": 2,
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

nbsphinx_allow_errors = True
