Installation
============

Dependencies
------------
DynaCrop SDK has the following dependencies:

.. code-block::

    geopandas>=0.7.0
    rasterio>=1.0a12
    numpy>=1.20.0
    requests>=2.20.0
    Fiona>=1.8.13


These might grow further along the development of the DynaCrop API and this SDK.

.. note::
    To be able to use this SDK, you will need your own API key. To obtain one, please visit `<https://dynacrop.space>`_ or contact our team at dynacrop@worldfrom.space for a free technical consultation.

Installation using pip
----------------------
Installation of the DynaCrop SDK is pretty simple with pip:

.. code-block:: Bash

    pip install dynacrop

Installation from source
------------------------
`The SDK's source code is available on  BitBucket <https://bitbucket.org/world_from_space/dynacrop-sdk/src/master/>`_, directed by `World from Space <https://worldfrom.space/>`_.


Clone the repository and install it using ``pip``:

.. code-block:: Bash

    git clone https://bitbucket.org/world_from_space/dynacrop-sdk.git
    pip install dynacrop-sdk/dynacrop


