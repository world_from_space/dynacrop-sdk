Package overview
================

This page gives reference of the SDK source code with doctypes, parameters and various useful notes to operate SDK conveniently.

We strongly advise to operate with anything but:

- all under the packages ``dynacrop.sdk`` and ``dynacrop.constants``
- the ``dynacrop.processing_request_base.ProcessingRequest`` class
- the ``dynacrop.config.Config`` class

These components are considered public under the ``dynacrop`` namespace and should be hinted by your IDE and accessible when you use statement:

.. code-block:: Python

    from dynacrop import *  # or better denominated


.. toctree::
   :maxdepth: 2
   :caption: Reference:

   source-code/config
   source-code/sdk
   source-code/api_handles
   source-code/constants
