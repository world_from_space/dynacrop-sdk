.. DynaCrop API SDK documentation master file, created by
   sphinx-quickstart on Mon Oct  4 16:01:54 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction
============

This Software Development Kit (SDK) is a library that enables access to `DynaCrop API <https://dynacrop.space>`_ services in Python.

.. note::
   The source code is available on `BitBucket <https://bitbucket.org/world_from_space/dynacrop-sdk/src>`_. You can file issues regarding the SDK on our `issue tracker <https://bitbucket.org/world_from_space/dynacrop-sdk/issues?status=new&status=open>`_.


.. figure:: dynacrop-logo.svg
    :width: 250
    :align: right
    :target: https://dynacrop.space
    :alt: dynacrop-logo

**DynaCrop** is an API service for field-related information based on satellite data. It provides advanced vegetation monitoring for agriculture, AI and for other companies running their own software solution.

|

DynaCrop is a service created by `World from Space <https://worldfrom.space/>`_, a remote sensing and geospatial company based in Brno, Czech Republic. While DynaCrop services are commercial, this SDK library is available under Public GNU license.


.. figure:: world-from-space-logo.png
    :width: 250
    :align: right
    :target: https://worldfrom.space/
    :alt: wfs-logo

.. note::
    To be able to use this SDK, you will need your own API key. To obtain one, please visit `<https://dynacrop.space>`_ or contact our team at dynacrop@worldfrom.space for a free technical consultation.


Documentation contents
----------------------

.. toctree::
   :maxdepth: 2

   self
   installation
   examples
   overview

