import pytest

from dynacrop.api_handles import APIObjectIterator
from dynacrop import PolygonV3


@pytest.mark.parametrize(
    "path, apiobject",
    [
        ("mocked_polygonv3.json", PolygonV3),
    ],
)
class TestMockedPolygon:
    def test_correct_instance(self, mocked_apiobject, apiobject):
        assert isinstance(mocked_apiobject, apiobject)

    def test_polygon_has_id(self, mocked_apiobject):
        assert mocked_apiobject.id

    @pytest.mark.parametrize(
        "noneditable_attr",
        [
            "id",
            "id_v2",
            "geometry",
            "area",
            "updated",
            "valid_observations",
            "cloud_cover",
            "allowed_products",
        ],
    )
    def test_polygon_noneditable_attrs(self, mocked_apiobject, noneditable_attr):
        with pytest.raises(AttributeError):
            setattr(mocked_apiobject, noneditable_attr, "nonsense")


@pytest.mark.parametrize("path, apiobject", [("mocked_polygon_list.json", PolygonV3)])
def test_polygon_list(mocked_apiobject_list):
    assert isinstance(mocked_apiobject_list, APIObjectIterator)
    assert isinstance(next(mocked_apiobject_list), PolygonV3)
